//
//  BeerProvider.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 27/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

class BeerProvider : BeerProtocol{

    //MARK - Proprieties
    let api = APIManager()
    
    //MARK: - List All
    func list(completion: @escaping APIResultCall) {

        api.open(url: Constants.url) { (result) in
            if result != nil{
                completion(result)
            }else{
                completion(nil)
            }
        }
    }
    
    //MARK: - Get by ID
    func byId(completion: @escaping APIResultCall) {
        api.open(url: Constants.url_details) { (result) in
            if result != nil{
                completion(result)
            }else{
                completion(nil)
            }
        }
    }
    
    //MARK: - Get Image
    func getImageFromUrl(url: URL, completion: @escaping APIResultImage) {
        api.getImageFromUrl(url: url) { (result) in
            if result != nil{
                completion(result)
            }else{
                completion(nil)
            }
        }
    }
}

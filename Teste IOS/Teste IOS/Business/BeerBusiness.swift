//
//  BeerBusiness.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 27/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

class BeerBusiness{
    
    //MARK: - Proprieties
    var provider: BeerProvider = BeerProvider()
    
    //MARK: - List ALL
    func list(completion: @escaping BeersCall){
        provider.list { (result) in
            if result != nil{
                do{
                    let json = JSONDecoder()
                    let beers = try json.decode([Beer].self, from: result!)
                    completion(beers)
                }catch{
                    completion(nil)
                }
                
            }else{
                completion(nil)
            }
        }
    }
    
    //MARK: - Get Image
    func getImageFromUrl(url_params: String, completion: @escaping APIResultImage){
        
        let url = URL(string: url_params)
        
        provider.getImageFromUrl(url: url!) { (result) in
            if result != nil{
                completion(result)
            }else{
                completion(nil)
            }
        }
    }
}

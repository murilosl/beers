//
//  BeerTableViewCell.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 30/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class BeerTableViewCell: UITableViewCell {

    @IBOutlet weak var image_beer: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var abv: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

//
//  APIRequest.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 26/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation
import Alamofire

struct API {
    //MARK: - Generic Request
    
    func request(completion: @escaping APIResultCall){
        Alamofire.request(Constants.url).responseData { (result) in
            
            if let dataReturn = result.value{
                completion(dataReturn)
            }else{
                completion(nil)
            }
        }
    }
}

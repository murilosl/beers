//
//  Beer.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 26/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

struct Beer : Codable{
    var id: Int?
    var name: String?
    var image_url: String?
    var abv: Double?
    var tagline: String?
    var description: String?
    var ibu: Double?
}

struct BeerDict : Codable {
    var beers: [Beer]?
}

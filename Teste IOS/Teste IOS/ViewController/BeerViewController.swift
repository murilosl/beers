//
//  BeerViewController.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 30/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class BeerViewController: UIViewController {
    
    //MARK: Proprieties
    var beer: Beer = Beer()
    let manage = BeerManager()
    //MARK: - Outlets
    @IBOutlet weak var image_beer: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var abv: UILabel!
    @IBOutlet weak var ibu: UILabel!
    @IBOutlet weak var beer_description: UITextView!
    
    //MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    //MARK: - Configure
    func configure(){
        manage.getImage(url: (beer.image_url)!) { (result) in
            if result != nil{
                DispatchQueue.main.async {
                    self.image_beer.image = result == nil ? #imageLiteral(resourceName: "movie_placeholder") : UIImage(data: result!)
                }
            }
        }
    
        self.name.text = beer.name
        self.tagline.text = beer.tagline
        self.abv.text = "\(beer.abv!)"
        self.ibu.text = "\(beer.ibu!)"
        self.beer_description.text = beer.description
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//
//  APIManager.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 27/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation
import Alamofire

class APIManager{
    
    //MARK: - Generic Request
    
    func open(url: String, completion: @escaping APIResultCall){
        
        Alamofire.request(url).responseData { (result) in
            if let dataReturn = result.value{
                completion(dataReturn)
            }else{
                completion(nil)
            }
        }
    }
    
    //MARK: - Download Image
    
    func getImageFromUrl(url: URL, completion: @escaping APIResultImage) {
        
        Alamofire.request(url).responseData { (result) in
            if let dataReturn = result.value{
                completion(dataReturn)
            }else{
                completion(nil)
            }
        }
    }
}

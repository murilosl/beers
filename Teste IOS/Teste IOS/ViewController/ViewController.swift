//
//  ViewController.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 26/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Proprieties
    let manager: BeerManager = BeerManager()
    var beers: [Beer] = []
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        DispatchQueue.main.async {
            self.manager.list { (beers) in
                if beers != nil{
                    self.beers = beers!
                    self.tableView.reloadData()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.beers.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTable", for: indexPath) as! BeerTableViewCell

        let beer = self.beers[indexPath.row]
        
        cell.title.text = beer.name
        cell.abv.text = "\(beer.abv!)"
        
        manager.getImage(url: beer.image_url!) { (result) in
            if result != nil{
                DispatchQueue.main.async {
                    cell.image_beer.image = result == nil ? #imageLiteral(resourceName: "movie_placeholder") : UIImage(data: result!)
                }
            }
        }

        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show"{
            let beerVC = segue.destination as! BeerViewController
            guard let row = tableView.indexPathForSelectedRow?.row else { return }
            
            let beer = self.beers[row]
            beerVC.beer = beer
        }
    }
}

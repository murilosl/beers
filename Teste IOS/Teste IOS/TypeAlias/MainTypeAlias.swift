//
//  MainTypeAlias.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 26/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

typealias APIResultCall = (Data?) -> ()
typealias APIResultImage = (Data?) -> ()
typealias BeerCall = (Beer?) -> ()
typealias BeersCall = ([Beer]?) -> ()

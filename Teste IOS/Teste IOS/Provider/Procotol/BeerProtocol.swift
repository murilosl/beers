//
//  BeerProtocol.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 26/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

protocol BeerProtocol {
    func list(completion: @escaping APIResultCall)
    func byId(completion: @escaping APIResultCall)
    func getImageFromUrl(url: URL, completion: @escaping APIResultImage)
}

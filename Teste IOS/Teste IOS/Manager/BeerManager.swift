//
//  BeerManager.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 27/09/2018.
//  Copyright © 2018 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

class BeerManager{
    
    //MARK: - Proprieties
    var business: BeerBusiness = BeerBusiness()
    
    //MARK: - List
    func list(completion: @escaping BeersCall){
        business.list(completion: completion)
    }
    
    //MARK: Get Image
    func getImage(url: String, completion: @escaping APIResultImage){
        business.getImageFromUrl(url_params: url, completion: completion)
    }
}
